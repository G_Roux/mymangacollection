# <img src="assets/ic_launcher.png" alt="drawing" height="70"/> MyMangaCollection

[![pipeline status](https://gitlab.com/G_Roux/mymangacollection/badges/master/pipeline.svg)](https://gitlab.com/G_Roux/mymangacollection/commits/master)

Personal project.
Follow and track your manga collection easily with a simple Flutter application using Kitsu API.

You can browse manga throught those available on Kistu API and save them thanks to a SQLite database to keep track of them.

## TODO

* Check for manga updates
* Try with more up-to-date API

## Screenshots

<img src="flutter_01.png" alt="drawing" height="400"/>
<img src="flutter_02.png" alt="drawing" height="400"/>
<img src="flutter_03.png" alt="drawing" height="400"/>

## Install

### Android

Download and install the latest release APK [here](https://gitlab.com/G_Roux/mymangacollection/-/releases).

### iOS

You need to build the application with flutter using:
```
flutter build ios && flutter install
```

You need to have a MacOS computer and an iOS device registered to perform this operation.

## How to Use

Simply browse the manga available on the API and add them to your collection. You will be able to keep track of the volume in your collection easily.

## Dependencies

* [http](https://pub.dev/packages/html)
* [cached_network_image](https://pub.dev/packages/cached_network_image)
* [path_provider](https://pub.dev/packages/path_provider)
* [flutter_launcher_icons](https://pub.dev/packages/flutter_launcher_icons)
* [dynamic_theme](https://pub.dev/packages/dynamic_theme)
* [sqflite](https://pub.dev/packages/sqflite)

## Thanks

* [Kitsu Api](https://kitsu.docs.apiary.io/#) - For the API
* [Manga Rock](https://mangarock.com/) - For the design inspiration
