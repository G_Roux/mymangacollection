import 'package:flutter/material.dart';
import 'package:my_manga_collection/Database/DatabaseHelper.dart';
import 'package:my_manga_collection/Models/MangaCollection.dart';
import 'package:my_manga_collection/utils/MangaTileWidget.dart';

class MyMangaCollection extends StatefulWidget {
  final MangaCollection myMangaList;
  final DatabaseHelper dbHelper;

  MyMangaCollection(this.myMangaList, this.dbHelper);

  @override
  State<StatefulWidget> createState() => _MyMangaCollectionState(myMangaList);
}

class _MyMangaCollectionState extends State<MyMangaCollection> {
  final MangaCollection myMangaList;
  final TextEditingController _filter = TextEditingController();

  String _searchText = "";
  MangaCollection _filteredList;
  Widget _appBarTitle;
  Icon _searchIcon = Icon(Icons.search);

  _MyMangaCollectionState(this.myMangaList) {
    _filter.addListener(() {
      if (_filter.text.isEmpty) {
        setState(() {
          _searchText = "";
          _filteredList = myMangaList;
        });
      } else {
        setState(() {
          _searchText = _filter.text;
        });
      }
    });
  }

  _searchPressed() {
    setState(() {
      if (_searchIcon.icon == Icons.search) {
        _searchIcon = Icon(Icons.close);
        _appBarTitle = TextField(
          controller: _filter,
          autofocus: true,
          decoration: InputDecoration(hintText: "Search..."),
        );
      } else {
        _searchIcon = Icon(Icons.search);
        _appBarTitle = Text("Collection");
        _filteredList = myMangaList;
        _filter.clear();
      }
    });
  }

  Widget _buildList() {
    _filteredList = myMangaList;
    if (_searchText.isNotEmpty) {
      MangaCollection tmpList = MangaCollection();
      for (int i = 0; i < _filteredList.length; i++) {
        var manga = _filteredList.mangaAtIndex(i);
        if (manga.title.toLowerCase().contains(_searchText.toLowerCase())) {
          tmpList.addManga(manga);
        }
      }
      _filteredList = tmpList;
    }
    return ListView.builder(
      physics: BouncingScrollPhysics(),
      shrinkWrap: true,
      itemCount: _filteredList.length,
      itemBuilder: (context, index) {
        return MangaTileWidget(
          _filteredList.mangaAtIndex(index),
          _filteredList,
          widget.dbHelper,
        );
      },
    );
  }

  @override
  void initState() {
    super.initState();
    _filteredList = myMangaList;
    _appBarTitle = Text("Collection");
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: _appBarTitle,
        backgroundColor: Theme.of(context).primaryColor,
        actions: <Widget>[
          IconButton(
            icon: _searchIcon,
            onPressed: _searchPressed,
          )
        ],
      ),
      body: Container(
        padding: const EdgeInsets.all(8.0),
        child: _buildList(),
      ),
    );
  }
}
