import 'package:dynamic_theme/dynamic_theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:my_manga_collection/HomePage/HomePage.dart';
import 'package:my_manga_collection/style/Theme.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    SystemChrome.setEnabledSystemUIOverlays([]);
    return DynamicTheme(
      defaultBrightness: Brightness.light,
      data: (brightness) => brightness == Brightness.light
          ? MyTheme.defaultTheme
          : MyTheme.darkTheme,
      themedWidgetBuilder: (context, theme) {
        return MaterialApp(
          title: 'My Manga Collection',
          theme: theme,
          home: HomePage(),
        );
      },
    );
  }
}
