import 'dart:convert';
import 'package:http/http.dart' as http;

class KitsuApi {
  final http.Client _client = http.Client();
  final String apiEndpoint = "https://kitsu.io/api/edge/";

  @override
  Future<List> getMangaWithText(String mangaName) async {
    if (mangaName == null || mangaName.isEmpty) return null;
    String uri = apiEndpoint + "manga?filter[text]=$mangaName";
    String encoded = Uri.encodeFull(uri);
    try {
      return await _client
          .get(encoded)
          .then((res) => res.body)
          .then(json.decode)
          .then((json) => json['data']);
    } catch (e) {
      print(e.toString());
      throw e.toString();
    }
  }

  @override
  Future<Map<String, dynamic>> getMangaWithId(String id) async {
    if (id == null || id.isEmpty) return null;
    String uri = apiEndpoint + "manga/$id";
    String encoded = Uri.encodeFull(uri);
    try {
      return await _client
          .get(encoded)
          .then((res) => res.body)
          .then(json.decode)
          .then((json) => json['data']);
    } catch (e) {
      print(e.toString());
      throw e.toString();
    }
  }
}
