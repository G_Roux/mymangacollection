import 'package:flutter/material.dart';

const PrimaryColor = const Color(0xFF2196F3);
const PrimaryColorLight = const Color(0xFF4cb0af);
const PrimaryColorDark = const Color(0xFF005354);

const SecondaryColor = const Color(0xFFb2dfdb);

const Background = const Color(0xfff5f5f5);
const TextColor = const Color(0xFF004d40);
const DarkBackground = const Color(0xFF222727);
const DarkChipSelected = const Color(0xFF82ada9);

class MyTheme {
  static final ThemeData defaultTheme = _buildMyTheme();
  static final ThemeData darkTheme = _buildDarkTheme();

  static ThemeData _buildMyTheme() {
    final ThemeData base = ThemeData.light();
    return base.copyWith(
      accentColorBrightness: Brightness.dark,
      primaryColor: PrimaryColor,
      primaryColorBrightness: Brightness.dark,
      buttonTheme: base.buttonTheme.copyWith(
        textTheme: ButtonTextTheme.primary,
      ),
      scaffoldBackgroundColor: Background,
      cardColor: Background,
      backgroundColor: Background,
    );
  }

  static ThemeData _buildDarkTheme() {
    final ThemeData base = ThemeData.dark();
    return base.copyWith(
      chipTheme: base.chipTheme.copyWith(
        secondarySelectedColor: DarkChipSelected,
      ),
    );
  }
}
