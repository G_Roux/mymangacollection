import 'dart:convert';
import 'package:my_manga_collection/Database/DatabaseHelper.dart';

class MangaModel {
  final String id;
  final String link;
  final String synopsis;
  final String title;
  final Map<String, String> images;
  final int chapterCount;
  final int volumeCount;

  bool isFav;
  List<String> volumeOwned;

  String _getImgUrl(String imgSize) {
    return this.images[imgSize] == null
        ? this.images.values.toList().first
        : this.images[imgSize];
  }

  String get tinyImgUrl => _getImgUrl('tiny');
  String get smallImgUrl => _getImgUrl('small');
  String get mediumImgUrl => _getImgUrl('medium');
  String get largeImgUrl => _getImgUrl('large');
  String get originalImgUrl => _getImgUrl('original');

  MangaModel({
    this.id,
    this.link,
    this.synopsis,
    this.title,
    this.images,
    this.chapterCount,
    this.volumeCount,
    this.isFav: false,
    this.volumeOwned,
  });

  factory MangaModel.fromSqlite(Map<String, dynamic> query) {
    Map<String, String> images = Map();
    images['tiny'] = query[DatabaseHelper.columnImgTiny];
    images['small'] = query[DatabaseHelper.columnImgSmall];
    images['medium'] = query[DatabaseHelper.columnImgMed];
    images['large'] = query[DatabaseHelper.columnImgLarge];
    images['original'] = query[DatabaseHelper.columnImgOrin];

    String volOwnStr = query[DatabaseHelper.columnVolOwn];

    return MangaModel(
      id: query[DatabaseHelper.columnMangaId],
      link: query[DatabaseHelper.columnLink],
      synopsis: query[DatabaseHelper.columnSynopsis],
      title: query[DatabaseHelper.columnTitle],
      images: images,
      chapterCount: query[DatabaseHelper.columnChapter],
      volumeCount: query[DatabaseHelper.columnVol],
      isFav: true,
      volumeOwned: volOwnStr.split(','),
    );
  }

  factory MangaModel.fromJson(Map<String, dynamic> json, {bool isFav}) {
    String title = json['attributes']['canonicalTitle'];

    Map<String, String> images = Map();
    images['tiny'] = json['attributes']['posterImage']['tiny'];
    images['small'] = json['attributes']['posterImage']['small'];
    images['medium'] = json['attributes']['posterImage']['medium'];
    images['large'] = json['attributes']['posterImage']['large'];
    images['original'] = json['attributes']['posterImage']['original'];

    List<String> volumeOwned = List();
    if (json['attributes']['volumeOwned'] != null) {
      json['attributes']['volumeOwned'].forEach((vol) {
        volumeOwned.add(vol);
      });
    }

    return MangaModel(
      id: json['id'],
      link: json['links']['self'],
      synopsis: json['attributes']['synopsis'],
      title: title.replaceAll(RegExp(r'[^\s\w]'), ''),
      chapterCount: json['attributes']['chapterCount'],
      volumeCount: json['attributes']['volumeCount'],
      images: images,
      volumeOwned: volumeOwned,
      isFav: isFav == null ? false : isFav,
    );
  }

  String toJson() {
    var convertObj = {
      'id': this.id,
      'links': {'self': this.link},
      'attributes': {
        'synopsis': this.synopsis,
        'canonicalTitle': this.title,
        'chapterCount': this.chapterCount,
        'volumeCount': this.volumeCount,
        'posterImage': {
          'tiny': this.tinyImgUrl,
          'small': this.smallImgUrl,
          'medium': this.mediumImgUrl,
          'large': this.largeImgUrl,
          'original': this.originalImgUrl
        },
        'isFav': this.isFav,
        'volumeOwned': this.volumeOwned
      }
    };
    return jsonEncode(convertObj);
  }

  Map<String, dynamic> toSqlite() {
    Map<String, dynamic> row = {
      DatabaseHelper.columnMangaId: this.id,
      DatabaseHelper.columnLink: this.link,
      DatabaseHelper.columnSynopsis: this.synopsis,
      DatabaseHelper.columnTitle: this.title,
      DatabaseHelper.columnImgTiny: this.tinyImgUrl,
      DatabaseHelper.columnImgSmall: this.smallImgUrl,
      DatabaseHelper.columnImgMed: this.mediumImgUrl,
      DatabaseHelper.columnImgLarge: this.largeImgUrl,
      DatabaseHelper.columnImgOrin: this.originalImgUrl,
      DatabaseHelper.columnChapter: this.chapterCount,
      DatabaseHelper.columnVol: this.volumeCount,
      DatabaseHelper.columnVolOwn: this.volumeOwned.join(','),
    };
    return row;
  }
}
