import 'package:my_manga_collection/Models/MangaModel.dart';

class MangaCollection {
  List<MangaModel> _mangaList = [];

  int get length => _mangaList.length;

  set content(List<MangaModel> mangaList) {
    _mangaList.clear();
    _mangaList.addAll(mangaList);
  }

  bool hasManga(String mangaId) {
    for (var ownedManga in _mangaList) {
      if (mangaId == ownedManga.id) return true;
    }
    return false;
  }

  MangaModel mangaAtIndex(int index) => _mangaList[index];
  MangaModel getManga(String mangaId) {
    for (MangaModel mangaOwned in _mangaList) {
      if (mangaId == mangaOwned.id) return mangaOwned;
    }
    return null;
  }

  void addManga(MangaModel manga) {
    if (!_mangaList.contains(manga)) _mangaList.add(manga);
  }

  void removeManga(MangaModel manga) {
    _mangaList.remove(manga);
  }
}
