import 'package:flutter/material.dart';
import 'package:my_manga_collection/utils/changeBrightness.dart';

class SettingsPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _SettingsPageState();
}

class _SettingsPageState extends State<SettingsPage> {
  @override
  Widget build(BuildContext context) {
    bool darkMode = Theme.of(context).brightness == Brightness.dark;
    return Container(
      padding: const EdgeInsets.all(16.0),
      child: ListView(
        shrinkWrap: true,
        children: <Widget>[
          ListTile(
            leading: Icon(Icons.lightbulb_outline),
            title: Text("Dark Mode"),
            trailing: Switch(
              value: darkMode,
              onChanged: (bool value) {
                setState(() {
                  darkMode = value;
                  changeBrightness(context, darkMode);
                });
              },
            ),
          ),
        ],
      ),
    );
  }
}
