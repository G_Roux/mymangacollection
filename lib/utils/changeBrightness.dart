import 'package:dynamic_theme/dynamic_theme.dart';
import 'package:flutter/material.dart';

void changeBrightness(BuildContext context, bool darkMode) {
  DynamicTheme.of(context)
      .setBrightness(darkMode ? Brightness.dark : Brightness.light);
}
