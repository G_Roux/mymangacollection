import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:my_manga_collection/Database/DatabaseHelper.dart';
import 'package:my_manga_collection/MangaPage/MangaPage.dart';
import 'package:my_manga_collection/Models/MangaCollection.dart';
import 'package:my_manga_collection/Models/MangaModel.dart';

class MangaTileWidget extends StatefulWidget {
  final MangaModel manga;
  final MangaCollection mangaList;
  final DatabaseHelper dbHelper;

  MangaTileWidget(this.manga, this.mangaList, this.dbHelper);

  @override
  State<StatefulWidget> createState() => _MangaTileWidgetState();
}

class _MangaTileWidgetState extends State<MangaTileWidget> {
  _MangaTileWidgetState();

  @override
  Widget build(BuildContext context) {
    bool isInCollection = widget.mangaList.hasManga(widget.manga.id);
    MangaModel tmpManga = widget.manga;

    if (isInCollection) {
      tmpManga = widget.mangaList.getManga(widget.manga.id);
    }

    return ListTile(
      leading: CachedNetworkImage(
        imageUrl: tmpManga.tinyImgUrl,
        placeholder: (context, url) => CircularProgressIndicator(),
        errorWidget: (context, url, error) => Icon(Icons.error),
      ),
      title: Text(tmpManga.title),
      subtitle: Text(
          "Owned: ${tmpManga.volumeOwned == null ? 0 : tmpManga.volumeOwned.length}/${tmpManga.volumeCount}"),
      trailing: Icon(Icons.arrow_forward_ios),
      onTap: () => Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) =>
              MangaPage(tmpManga, widget.mangaList, widget.dbHelper),
        ),
      ),
    );
  }
}
