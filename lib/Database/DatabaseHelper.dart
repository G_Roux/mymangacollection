import 'dart:io';
import 'package:my_manga_collection/Models/MangaModel.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

class DatabaseHelper {
  static final _databaseName = "MangaCollection.db";
  static final _databaseVersion = 1;

  static final tableManga = "tableManga";

  static final columnId = "_id";
  static final columnMangaId = "mangaId";
  static final columnLink = "link";
  static final columnSynopsis = "synopsis";
  static final columnTitle = "title";
  static final columnImgTiny = "tiny";
  static final columnImgSmall = "small";
  static final columnImgMed = "medium";
  static final columnImgLarge = "large";
  static final columnImgOrin = "original";
  static final columnChapter = "chapterCount";
  static final columnVol = "volumeCount";
  static final columnVolOwn = "volumeOwned";

  DatabaseHelper._privateConstructor();
  static final DatabaseHelper instance = DatabaseHelper._privateConstructor();

  static Database _database;

  Future<Database> get database async {
    if (_database != null) return _database;
    _database = await _initDatabase();
    return _database;
  }

  Future<Database> _initDatabase() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, _databaseName);
    return await openDatabase(path,
        version: _databaseVersion, onCreate: _onCreate);
  }

  Future _onCreate(Database db, int version) async {
    await db.execute('''
      CREATE TABLE $tableManga (
      $columnId INTEGER PRIMARY KEY,
      $columnMangaId TEXT NOT NULL,
      $columnLink TEXT NOT NULL,
      $columnSynopsis TEXT,
      $columnTitle TEXT,
      $columnImgTiny TEXT,
      $columnImgSmall TEXT,
      $columnImgMed TEXT,
      $columnImgLarge TEXT,
      $columnImgOrin TEXT,
      $columnChapter INTEGER,
      $columnVol INTEGER,
      $columnVolOwn TEXT
    )
    ''');
  }

  Future<int> createManga(MangaModel manga) async {
    Database db = await instance.database;
    int id = await db.insert(tableManga, manga.toSqlite());
    return id;
  }

  Future<List<MangaModel>> readManga() async {
    Database db = await instance.database;
    List<MangaModel> mangaList = List();
    var mangaQueries = await db.query(tableManga);
    for (var mangaQuery in mangaQueries) {
      mangaList.add(MangaModel.fromSqlite(mangaQuery));
    }
    return mangaList;
  }

  Future<int> updateManga(MangaModel manga) async {
    Database db = await instance.database;
    var exists = await _entryExists(manga.id, db);
    if (exists != null) {
      int id = await db.update(tableManga, manga.toSqlite(),
          where: '$columnMangaId = ?', whereArgs: [manga.id]);
      return id;
    }
    return createManga(manga);
  }

  Future<int> deleteManga(MangaModel manga) async {
    Database db = await instance.database;
    return await db
        .delete(tableManga, where: '$columnMangaId = ?', whereArgs: [manga.id]);
  }

  Future<List<Map<String, dynamic>>> _entryExists(
      String mangaId, Database db) async {
    var rows = await db
        .query(tableManga, where: '$columnMangaId = ?', whereArgs: [mangaId]);
    return rows == null || rows.isEmpty ? null : rows;
  }
}
