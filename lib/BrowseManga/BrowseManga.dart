import 'package:flutter/material.dart';
import 'package:my_manga_collection/Api/KitsuApi.dart';
import 'package:my_manga_collection/BrowseManga/widgets/LoadingWidget.dart';
import 'package:my_manga_collection/Database/DatabaseHelper.dart';
import 'package:my_manga_collection/Models/MangaCollection.dart';
import 'package:my_manga_collection/Models/MangaModel.dart';
import 'package:my_manga_collection/utils/MangaTileWidget.dart';

class BrowseManga extends StatefulWidget {
  final MangaCollection myMangaList;
  final DatabaseHelper dbHelper;

  BrowseManga(this.myMangaList, this.dbHelper);

  @override
  State<StatefulWidget> createState() => _BrowseMangaState();
}

class _BrowseMangaState extends State<BrowseManga> {
  final KitsuApi _httpProvider = KitsuApi();

  bool _isLoading = false;
  List<MangaModel> _searchList = List();

  _searchMangaText(String onSubmitted) async {
    if (onSubmitted == null || onSubmitted.isEmpty) return;
    setState(() {
      _isLoading = true;
    });
    _searchList.clear();
    List allMangas = await _httpProvider.getMangaWithText(onSubmitted);
    allMangas.forEach((manga) => _searchList.add(MangaModel.fromJson(manga)));
    setState(() {
      _isLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        children: <Widget>[
          Padding(
            padding:
                const EdgeInsets.only(bottom: 16.0, left: 16.0, right: 16.0),
            child: TextField(
              autocorrect: false,
              decoration: InputDecoration(
                hintText: "Manga name...",
              ),
              onSubmitted: _searchMangaText,
            ),
          ),
          _isLoading
              ? LoadingWidget()
              : Expanded(
                  child: ListView.builder(
                    physics: BouncingScrollPhysics(),
                    shrinkWrap: true,
                    itemCount: _searchList.length,
                    itemBuilder: (BuildContext context, int index) {
                      return MangaTileWidget(
                        _searchList[index],
                        widget.myMangaList,
                        widget.dbHelper,
                      );
                    },
                  ),
                ),
        ],
      ),
    );
  }
}
