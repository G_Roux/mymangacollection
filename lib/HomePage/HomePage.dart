import 'package:flutter/material.dart';
import 'package:my_manga_collection/BrowseManga/BrowseManga.dart';
import 'package:my_manga_collection/Database/DatabaseHelper.dart';
import 'package:my_manga_collection/Models/MangaCollection.dart';
import 'package:my_manga_collection/Models/MangaModel.dart';
import 'package:my_manga_collection/MyMangaPage/MyMangaCollection.dart';
import 'package:my_manga_collection/SettingsPage/SettingsPage.dart';

class HomePage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final _myMangaList = MangaCollection();
  final _dbHelper = DatabaseHelper.instance;

  int _currentIndex = 1;
  bool _isLoading = true;

  _loadSavedManga() async {
    List<MangaModel> mangaInDb = await _dbHelper.readManga();
    mangaInDb.forEach((manga) => _myMangaList.addManga(manga));
    setState(() {
      _isLoading = false;
    });
  }

  @override
  void initState() {
    super.initState();
    _loadSavedManga();
  }

  void onTabTapped(int index) {
    setState(() {
      _currentIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    final List<Widget> _bottomBarElem = [
      BrowseManga(_myMangaList, _dbHelper),
      MyMangaCollection(_myMangaList, _dbHelper),
      SettingsPage(),
    ];

    return Scaffold(
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: _currentIndex,
        onTap: onTabTapped,
        fixedColor: Colors.blue,
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.add),
            title: Text("Add Manga"),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.book),
            title: Text("Collection"),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.settings),
            title: Text("Settings"),
          )
        ],
      ),
      body: _isLoading
          ? Center(child: CircularProgressIndicator())
          : SafeArea(child: _bottomBarElem[_currentIndex]),
    );
  }
}
