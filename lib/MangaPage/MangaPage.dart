import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:my_manga_collection/Database/DatabaseHelper.dart';
import 'package:my_manga_collection/MangaPage/widgets/MultiSelectVol.dart';
import 'package:my_manga_collection/Models/MangaCollection.dart';
import 'package:my_manga_collection/Models/MangaModel.dart';

class MangaPage extends StatefulWidget {
  final MangaModel manga;
  final MangaCollection mangaList;
  final DatabaseHelper dbHelper;

  MangaPage(this.manga, this.mangaList, this.dbHelper);

  @override
  State<StatefulWidget> createState() => _MangaPageState(mangaList);
}

class _MangaPageState extends State<MangaPage> {
  final MangaCollection mangaList;

  List<String> selectedVolList;
  MangaModel manga;

  _MangaPageState(this.mangaList);

  @override
  void initState() {
    super.initState();
    manga = widget.manga;
    if (mangaList.hasManga(manga.id)) {
      setState(() {
        manga = mangaList.getManga(manga.id);
      });
    }
  }

  _addManga() async {
    setState(() {
      manga.isFav = true;
      manga.volumeOwned = selectedVolList;
      mangaList.addManga(manga);
    });
    await widget.dbHelper.createManga(manga);
  }

  _removeManga() async {
    setState(() {
      mangaList.removeManga(manga);
      manga.isFav = false;
      manga.volumeOwned.clear();
    });
    await widget.dbHelper.deleteManga(manga);
  }

  _showAddMangaDialog() {
    var mangaList = List<String>.generate(
        manga.volumeCount, (index) => (index + 1).toString());
    showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(16.0)),
          title: Text("Volume Owned"),
          content:
              MultiSelectVol(mangaList, onSelectionChanged: (selectedList) {
            setState(() {
              selectedVolList = selectedList;
            });
          }),
          actions: <Widget>[
            FlatButton(
              child: Text("Add"),
              onPressed: () {
                _addManga();
                Navigator.pop(context);
              },
            ),
            FlatButton(
              child: Text("Cancel"),
              onPressed: () => Navigator.pop(context),
            ),
          ],
        );
      },
    );
  }

  @override
  void dispose() {
    super.dispose();
    SystemChrome.setSystemUIOverlayStyle(
        SystemUiOverlayStyle.dark.copyWith(statusBarColor: Colors.transparent));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).primaryColor,
        actions: <Widget>[
          IconButton(
            icon: Icon(!manga.isFav ? Icons.favorite_border : Icons.favorite),
            onPressed: () {
              if (manga.isFav)
                _removeManga();
              else
                _showAddMangaDialog();
            },
          ),
        ],
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          physics: BouncingScrollPhysics(),
          child: Column(
            children: <Widget>[
              Container(
                decoration: BoxDecoration(
                    image: DecorationImage(
                        image: CachedNetworkImageProvider(manga.mediumImgUrl),
                        fit: BoxFit.cover)),
                padding: const EdgeInsets.all(8.0),
                alignment: Alignment.center,
                child: Card(
                  child: Container(
                    padding: const EdgeInsets.all(6.0),
                    child: CachedNetworkImage(
                      imageUrl: manga.tinyImgUrl,
                      placeholder: (context, url) =>
                          CircularProgressIndicator(),
                      errorWidget: (context, url, error) => Icon(Icons.error),
                    ),
                  ),
                ),
              ),
              SizedBox(height: 15),
              Text(manga.title,
                  style: TextStyle(fontSize: 24.0, fontWeight: FontWeight.bold),
                  textAlign: TextAlign.center),
              SizedBox(height: 15),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16.0),
                child: Text(manga.synopsis, textAlign: TextAlign.center),
              ),
              SizedBox(height: 15),
              Text(
                  "${manga.chapterCount} Chapters - ${manga.volumeCount} Volumes"),
              SizedBox(height: 15),
              manga.volumeOwned != null && manga.volumeOwned.length > 0
                  ? Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 16.0),
                      child: MultiSelectVol(
                        List<String>.generate(manga.volumeCount,
                            (index) => (index + 1).toString()),
                        onSelectionChanged: (selectedList) async {
                          setState(() {
                            manga.volumeOwned = selectedList;
                          });
                          await widget.dbHelper.updateManga(manga);
                        },
                        alreadySelected: manga.volumeOwned,
                      ),
                    )
                  : Container(),
            ],
          ),
        ),
      ),
    );
  }
}
