import 'package:flutter/material.dart';

class MultiSelectVol extends StatefulWidget {
  final List<String> volumeList;
  final Function(List<String>) onSelectionChanged;
  final List<String> alreadySelected;
  final bool needList;

  MultiSelectVol(this.volumeList,
      {this.onSelectionChanged, this.alreadySelected, this.needList: false});

  @override
  State<StatefulWidget> createState() => _MultiSelectVolState();
}

class _MultiSelectVolState extends State<MultiSelectVol> {
  List<String> selectedChoices = List();

  _buildChoiceList() {
    List<Widget> choices = List();
    widget.volumeList.forEach((item) {
      choices.add(Container(
        padding: const EdgeInsets.all(2.0),
        child: ChoiceChip(
          label: Text(item),
          selected: selectedChoices.contains(item),
          onSelected: (selected) {
            setState(() {
              selectedChoices.contains(item)
                  ? selectedChoices.remove(item)
                  : selectedChoices.add(item);
              widget.onSelectionChanged(selectedChoices);
            });
          },
        ),
      ));
    });
    return choices;
  }

  @override
  void initState() {
    super.initState();
    if (widget.alreadySelected != null && widget.alreadySelected.isNotEmpty) {
      selectedChoices.addAll(widget.alreadySelected);
    }
  }

  @override
  Widget build(BuildContext context) {
    if (widget.needList) {
      return ListView(
        children: <Widget>[Wrap(children: _buildChoiceList())],
      );
    } else {
      return Wrap(children: _buildChoiceList());
    }
  }
}
